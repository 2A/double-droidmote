# DroidMote #

An android application for telecontrolling a [Double](https://www.doublerobotics.com/) robot.

Should work in conjunction with [iBrains](https://bitbucket.org/2A/double-ibrains) and [server](https://bitbucket.org/2A/double-server).

Based on [OpenWebRTC Android NativeCall example](https://github.com/EricssonResearch/openwebrtc-examples/tree/master/android/NativeCall).

### How to install

Open project in [Android. Studio](https://developer.android.com/studio/), fix paths in `settings.gradle` if nessessary, build and install to a device. 
