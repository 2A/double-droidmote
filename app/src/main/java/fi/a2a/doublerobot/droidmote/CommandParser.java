package fi.a2a.doublerobot.droidmote;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONStringer;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by doublea on 18.8.2016.
 */

public class CommandParser {
    public static final String TAG = "CommandParser";

    private SignalingChannel.PeerChannel mPeerChannel;

    public void setPeerChannel(SignalingChannel.PeerChannel peerChannel) {
        mPeerChannel = peerChannel;
    }

    public void parkOn() {
        JSONObject datajson = new JSONObject();
        JSONObject msgjson = new JSONObject();
        JSONArray cmd = new JSONArray();
        cmd.put("park");
        cmd.put("on");
        try {
            msgjson.put("command", cmd);
            datajson.put("data", msgjson);
        } catch (Exception e) {
            Log.e(TAG, "Oh the horror! It's a terrible Error!");
        }
        Log.d(TAG, datajson.toString());
        if (mPeerChannel != null) {
            mPeerChannel.send(datajson);
        }
    }

    public void parkOff() {
        JSONObject datajson = new JSONObject();
        JSONObject msgjson = new JSONObject();
        JSONArray cmd = new JSONArray();
        cmd.put("park");
        cmd.put("off");
        try {
            msgjson.put("command", cmd);
            datajson.put("data", msgjson);
        } catch (Exception e) {
            Log.e(TAG, "Oh the horror! It's a terrible Error!");
        }
        Log.d(TAG, datajson.toString());
        if (mPeerChannel != null) {
            mPeerChannel.send(datajson);
        }
    }

    public void drive(double speed, double turn) {
        JSONObject datajson = new JSONObject();
        JSONObject msgjson = new JSONObject();
        JSONArray cmd = new JSONArray();
        try {
            cmd.put("drive");
            cmd.put(Double.toString(speed));
            cmd.put(Double.toString(turn));
        } catch (Exception e) {
            Log.e(TAG, "Error parsing command to JSON array!");
        }
        try {
            msgjson.put("command", cmd);
            datajson.put("data", msgjson);
        } catch (Exception e) {
            Log.e(TAG, "Oh the horror! It's a terrible Error!");
        }
        Log.d(TAG, datajson.toString());
        if (mPeerChannel != null) {
            mPeerChannel.send(datajson);
        }
        ping();
    }

    public void ping() {
        long ptime = System.currentTimeMillis();
        JSONObject datajson = new JSONObject();
        JSONObject msgjson = new JSONObject();
        try {
            msgjson.put("ping", ptime);
            datajson.put("data", msgjson);
        } catch (Exception e) {
            Log.e(TAG, "Oh the horror! It's a terrible Error!");
        }
        Log.d(TAG, datajson.toString());
        if (mPeerChannel != null) {
            mPeerChannel.send(datajson);
        }
    }
}
