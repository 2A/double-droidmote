package fi.a2a.doublerobot.droidmote;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.text.LoginFilter;
import android.util.Log;

import android.os.Vibrator;
import android.view.WindowManager;

import static android.content.Context.SENSOR_SERVICE;

/**
 * Created by doublea on 18.8.2016.
 *

 Sensor event listener based on code from
 https://www.reddit.com/r/androiddev/comments/1av1la/simple_and_complete_android_device_rotation/

 *
 **/

public class Tracker implements SensorEventListener {

    public static final String TAG = "Tracker";

    private static final int DELAY = 200000; // us

    private SharedPreferences sharedPreferences;

    // Different mapping functions for convertind tilt to velocity/turn
    // 0 = linear, 1 = exponential,
    private int VelocityMappingFunction = 1;
    private int TurnMappingFunction = 0;

    // Amountof  "stand still" area, where speed/turn is considered 0.
    // This should help keeping the robot still.
    private double speedThreshold = 0.1;
    private double turnThreshold = 0.1;

    private float maxPitch;
    private float minPitch;
    private float maxRoll;
    private float minRoll;

    private SensorEventListener sensorEventListener;
    private SensorManager sensorManager;
    private Context ctx;

    private float gravRot[]=null; //for gravity rotational data
    private float magRot[]=null; //for magnetic rotational data
    private float accels[]=new float[3];
    private float mags[]=new float[3];
    private float[] values = new float[3];

    private float azimuth;
    private float pitch_;
    private float roll_;

    private boolean needInit;
    private int initCount;
    private boolean calibMaxRoll;
    private boolean calibMinRoll;
    private boolean calibMaxPitch;
    private boolean calibMinPitch;

    private float pitchInit;
    private float pitchDelta; // Speed
    private float rollInit;
    private float rollDelta; // turn
    private float azmInit;
    private float azmDelta; //

    public double speed;
    public double turn;
    public double pitch;
    public double roll;

    // private Runnable uiCallBack;

    // private SignalingChannel.PeerChannel mPeerChannel;

    CommandParser parser;
    DroidMoteActivity activity;

    private long waitTime;
    private int waitCount;

    private static int VIBRA_TIME = 50;
    private static int VIBRA_TIMEOUT = 100;
    private long lastVibra;
    private Vibrator vibra;

    public Tracker(DroidMoteActivity a, Context ctx) {
        needInit = true;
        initCount = 5;
        calibMaxRoll = true;
        calibMinRoll = true;
        calibMaxPitch = true;
        calibMinPitch = true;

        activity = a;
        this.ctx = ctx;
        this.sensorManager = (SensorManager) ctx.getSystemService(SENSOR_SERVICE);

        parser = new CommandParser();

        waitTime = DELAY;
        waitCount = 0;

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(ctx);
        lastVibra = 0;
        this.vibra = (Vibrator) this.ctx.getSystemService(Context.VIBRATOR_SERVICE);

        pitchInit = (float)(Math.PI/6.0);
        rollInit = (float)(Math.PI/2.0);
    }

    private void vibrate() {
        int time = (int)(System.nanoTime()/1000000);
        if (time > lastVibra + VIBRA_TIMEOUT) {
            vibra.vibrate(VIBRA_TIME);
            lastVibra = time;
        }
    }

    public void setPeerChannel(SignalingChannel.PeerChannel peerChannel) {
        // mPeerChannel = peerChannel;
        parser.setPeerChannel(peerChannel);
    }

    /*
    public void setUICallBack(Runnable runnable) {
        uiCallBack = runnable;
    }
    */

    public void park() {
        parser.parkOn();
    }
    public void unpark() {
        parser.parkOff();
    }

    public void start() {
        this.sensorManager.registerListener(this, this.sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), DELAY); // ,SensorManager.SENSOR_DELAY_NORMAL);
        this.sensorManager.registerListener(this, this.sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD), DELAY); // ,SensorManager.SENSOR_DELAY_NORMAL);
    }

    public void stop() {
        this.sensorManager.unregisterListener(this, this.sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER));
        this.sensorManager.unregisterListener(this, this.sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD));
        parser.drive(0, 0);
        needInit = true;
        initCount = 5;
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // ...
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        switch (event.sensor.getType())
        {
            case Sensor.TYPE_MAGNETIC_FIELD:
                mags = event.values.clone();
                break;
            case Sensor.TYPE_ACCELEROMETER:
                accels = event.values.clone();
                break;
        }

        if (mags != null && accels != null) {
            gravRot = new float[9];
            magRot = new float[9];
            SensorManager.getRotationMatrix(gravRot, magRot, accels, mags);

            float[] outR = new float[9];

            int rotation = ((WindowManager) ctx.getApplicationContext().getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getRotation();
            int rotation2 = ((WindowManager) ctx.getApplicationContext().getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getRotation();
            if(rotation == 0) {// Default display rotation is portrait
                Log.d(TAG, "ROT 0");
                SensorManager.remapCoordinateSystem(gravRot, SensorManager.AXIS_X, SensorManager.AXIS_Z, outR);
                // SensorManager.remapCoordinateSystem(Rmat, SensorManager.AXIS_MINUS_X, SensorManager.AXIS_Y, R2);
            }
            else   // Default display rotation is landscape
            {
                Log.d(TAG, "ROT N:  "  + Integer.toString(rotation) );
                SensorManager.remapCoordinateSystem(gravRot, SensorManager.AXIS_MINUS_X,SensorManager.AXIS_MINUS_Z, outR);
            }
                // SensorManager.remapCoordinateSystem(Rmat, SensorManager.AXIS_Y, SensorManager.AXIS_MINUS_X, R2);

            SensorManager.getOrientation(outR, values);

            azimuth = values[0]; //  * 57.2957795f; //looks like we don't need this one
            pitch_ =values[1];  //  * 57.2957795f;
            roll_ = values[2]; //  * 57.2957795f;
            float grav = accels[0];

            mags = null;      // retrigger the loop when things are repopulated
            accels = null;   // retrigger the loop when things are repopulated


            Log.d(TAG, " Got orientation: pitchD: " + Double.toString(pitch_) + " rollD: " + Double.toString(roll_) + "azim: " + Float.toString(grav));
            // if (needInit == true) {
            if (initCount > 0) {
                Log.d(TAG, "INIT TRACKING");
                azmInit = azimuth;
                //pitchInit = sharedPreferences.getFloat("ZERO_PITCH_PREF", pitch_);

                // pitchInit = pitch_;
                // rollInit = roll_;
                // rollInit = -(float)(Math.PI/2.0); // landscape leveled.
                initCount = initCount -1;
                needInit = false;
                waitTime = System.nanoTime();
                pitchDelta = 0;
                rollDelta = 0;
                waitCount = 0;
            }

            if (grav > 0) {
                pitchDelta += pitchInit - pitch_;
            }
            else {
                pitchDelta += pitchInit - (Math.PI-pitch_);
            }
            rollDelta += rollInit - roll_;
            waitCount += 1;

            /*
            if (calibMaxRoll) { maxRoll = rollDelta/waitCount; }
            if (calibMinRoll) { minRoll = rollDelta/waitCount; }
            if (calibMaxPitch) { maxPitch = pitchDelta/waitCount; }
            if (calibMinPitch) { minPitch = pitchDelta/waitCount; }
            */
        }
        // Use orientation to control robot
        if (initCount == 0) {
            // round numbers

            // Hack to delay sending messages
            // the delay set in SensonManager.registerListener does not seem to do anything
            if ((System.nanoTime()- waitTime)/1000 > DELAY) {

                pitchDelta = pitchDelta/waitCount;
                rollDelta = rollDelta/waitCount;

                double v = 0; // forward speed
                double t = 0; // turn speed

                float maxPitch_ = (float)(Math.PI/6.0 - Math.PI/60.0); // 30 deg
                float maxTurn_ = (float)(Math.PI/4.0- Math.PI/40.0);  // 45 deg

                if (pitchDelta >= 0) { v = (double)Math.round(100*pitchDelta/maxPitch_)/100; }
                else { v = (double)Math.round(100*pitchDelta/maxPitch_)/100; }
                if (rollDelta >= 0) { t = (double)Math.round(-100*rollDelta/maxTurn_)/100; }
                else { t = -(double)Math.round(100*rollDelta/maxTurn_)/100; }

                // if (pitchDelta >= 0) { v = (double)Math.round(100*pitchDelta/sharedPreferences.getFloat("MAX_PITCH_PREF", 0))/100; }
                // else { v = -(double)Math.round(100*pitchDelta/sharedPreferences.getFloat("MIN_PITCH_PREF", 0))/100; }
                // if (rollDelta >= 0) { t = (double)Math.round(100*rollDelta/sharedPreferences.getFloat("MAX_ROLL_PREF", 0))/100; }
                // else { t = -(double)Math.round(100*rollDelta/sharedPreferences.getFloat("MIN_ROLL_PREF", 0))/100; }

                // Log.d(TAG, " Got orientation: pitchD: " + Double.toString(v) + " rollD: " + Double.toString(t));

                // Linear mapping from angles to speed/turn with a threshold around zero.
                // The threshold may help stopping and keeping robot still.

                if (v > 0.0) {
                    v = v - speedThreshold;
                    if (v < 0.0) v = 0.0;
                    if (v > 1.0) {
                        v = 1.0;
                        vibrate();
                    }
                }
                else if (v < -0.0) {
                    v = v + speedThreshold;
                    if (v > 0.0) v = 0.0;
                    if (v < -1.0) {
                        v = -1.0;
                        vibrate();
                    }
                }
                else v = 0.0;

                if (t > 0.0) {
                    t = t - turnThreshold;
                    if (t < 0.0) t = 0.0;
                    if (t > 1.0) {
                        t = 1.0;
                        vibrate();
                    }
                }
                else if (t < -0.0) {
                    t = t + turnThreshold;
                    if (t > 0.0) t = 0.0;
                    if (t < -1.0) {
                        t = -1.0;
                        vibrate();
                    }
                }
                else t = 0.0;

                // Use exponential mapping according to preferences.
                // Exponentioal mapping might make control easier on small speeds
                int vmf = Integer.parseInt(sharedPreferences.getString("pref_map_function_vert", "0"));
                int hmf = Integer.parseInt(sharedPreferences.getString("pref_map_function_horiz", "0"));
                if (vmf == 1) { v = v * Math.abs(v); }
                if (hmf == 1) { t = t * Math.abs(t); }

                // round and fix direction
                v = -(double)Math.round(v*100)/100;
                t = (double)Math.round(t*100)/100;
                pitch = (double)Math.round(pitchDelta*10000)/10000;
                roll = (double)Math.round(rollDelta*10000)/10000;

                speed = v;
                turn = t;

                activity.runOnUiThread(activity.updateSpeedTurnUI);

                Log.d(TAG, "SPEED: " + Double.toString(v) + " TURN: " + Double.toString(t));
                parser.drive(v, t);
                waitTime = System.nanoTime();
                waitCount = 0;
                pitchDelta = 0;
                rollDelta = 0;
            }
        }
    }
}
