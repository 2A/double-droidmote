package fi.a2a.doublerobot.droidmote;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by doublea on 21.9.2016.
 */

public class TiltCalibrator extends Activity implements SensorEventListener, View.OnClickListener {
    private static final String TAG = "TiltCalibrator";

    private static final int DELAY = 200000; // us

    private float accels[]=new float[3];
    private float mags[]=new float[3];
    private float gravRot[]=null; //for gravity rotational data
    private float magRot[]=null; //for magnetic rotational data
    private float[] values = new float[3];

    private SensorEventListener sensorEventListener;
    private SensorManager sensorManager;
    // private Context ctx;

    private float pitch;
    private float roll;

    private SharedPreferences sharedPreferences;

    private Button rOKButton, lOKButton;
    private TextView instructionText;
    private TextView pitchText;
    private TextView rollText;

    private TextView zeroText;
    private TextView fwdText;
    private TextView bwdText;
    private TextView leftText;
    private TextView rightText;

    public enum CalibrationState {
        START,
        ZERO,
        MAX_PITCH,
        MAX_ROLL,
        MIN_PITCH,
        MIN_ROLL,
        FINISH
    };

    private CalibrationState cState;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "Start onCreate");
        cState = CalibrationState.START;
        super.onCreate(savedInstanceState);

        sensorManager = (SensorManager) this.getSystemService(SENSOR_SERVICE);

        setContentView(R.layout.calibration_layout);

        lOKButton = (Button) findViewById(R.id.leftButtonOK);
        rOKButton = (Button) findViewById(R.id.rightButtonOK);
        lOKButton.setOnClickListener(this);
        rOKButton.setOnClickListener(this);

        pitchText = (TextView) findViewById(R.id.textViewTCPitch);
        rollText = (TextView) findViewById(R.id.textViewTCRoll);

        instructionText = (TextView) findViewById(R.id.textViewTCInstruction);
        instructionText.setText("CLICK OK TO START");

        fwdText = (TextView) findViewById(R.id.textViewTCfwd);
        bwdText = (TextView) findViewById(R.id.textViewTCBack);
        zeroText = (TextView) findViewById(R.id.textViewTCMid);
        leftText = (TextView) findViewById(R.id.textViewTCleft);
        rightText = (TextView) findViewById(R.id.textViewTCRight);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        //
        zeroText.setText("Center: " + Float.toString(sharedPreferences.getFloat("ZERO_PITCH_PREF", 0)) + ", " + Float.toString(sharedPreferences.getFloat("ZERO_ROLL_PREF", 0)));
        fwdText.setText("Forward: " + Float.toString(sharedPreferences.getFloat("MAX_PITCH_PREF", 0)));
        bwdText.setText("Backwards: " + Float.toString(sharedPreferences.getFloat("MIN_PITCH_PREF", 0)));
        leftText.setText("Forward: " + Float.toString(sharedPreferences.getFloat("MAX_ROLL_PREF", 0)));
        rightText.setText("Forward: " + Float.toString(sharedPreferences.getFloat("MIN_ROLL_PREF", 0)));


    }

    @Override
    public void onClick(View v) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        float pr = 0;

        switch (cState){
            case START:
                instructionText.setText("Keep device in neutral/still position and hit OK.");

                zeroText.setText("Center: " + Float.toString(0) + ", " + Float.toString(0));
                fwdText.setText("Forward: " + Float.toString(0));
                bwdText.setText("Backwards: " + Float.toString(0));
                leftText.setText("Forward: " + Float.toString(0));
                rightText.setText("Forward: " + Float.toString(0));

                cState = CalibrationState.ZERO;
                start();
                break;
            case ZERO:
                editor.putFloat("ZERO_ROLL_PREF", roll);
                editor.putFloat("ZERO_PITCH_PREF", pitch);
                editor.commit();
                zeroText.setText("Center: " + Float.toString(sharedPreferences.getFloat("ZERO_PITCH_PREF", 0)) + ", " + Float.toString(sharedPreferences.getFloat("ZERO_ROLL_PREF", 0)));
                instructionText.setText("Tilt device forward to \"max speed\" position and hit OK.");
                cState = CalibrationState.MAX_PITCH;
                break;
            case MAX_PITCH:
                pr = pitch - sharedPreferences.getFloat("ZERO_PITCH_PREF", 0);
                editor.putFloat("MAX_PITCH_PREF", pr);
                editor.commit();
                fwdText.setText("Forward: " + Float.toString(sharedPreferences.getFloat("MAX_PITCH_PREF", 0)));
                instructionText.setText("Tilt device back to \"max reverse\" position and hit OK.");
                cState = CalibrationState.MIN_PITCH;
                break;
            case MIN_PITCH:
                pr = pitch - sharedPreferences.getFloat("ZERO_PITCH_PREF", 0);
                editor.putFloat("MIN_PITCH_PREF", pr);
                editor.commit();
                bwdText.setText("Backwards: " + Float.toString(sharedPreferences.getFloat("MIN_PITCH_PREF", 0)));
                instructionText.setText("Tilt device left to max turn position and hit OK.");
                cState = CalibrationState.MIN_ROLL;
                break;
            case MIN_ROLL:
                pr = roll - sharedPreferences.getFloat("ZERO_ROLL_PREF", 0);
                editor.putFloat("MAX_ROLL_PREF", pr);
                editor.commit();
                leftText.setText("Forward: " + Float.toString(sharedPreferences.getFloat("MAX_ROLL_PREF", 0)));
                instructionText.setText("Tilt device right to max turn position and hit OK.");
                cState = CalibrationState.MAX_ROLL;
                break;
            case MAX_ROLL:
                pr = roll - sharedPreferences.getFloat("ZERO_ROLL_PREF", 0);
                editor.putFloat("MIN_ROLL_PREF", pr);
                editor.commit();
                rightText.setText("Forward: " + Float.toString(sharedPreferences.getFloat("MIN_ROLL_PREF", 0)));
                instructionText.setText("All Done. Hit OK to recalibrate or back to return to preferences");
                cState = CalibrationState.FINISH;
                stop();
                break;
            case FINISH:
                instructionText.setText("Keep device in neutral/still position and hit OK.");
                cState = CalibrationState.ZERO;
                start();
                break;
            default:
                cState = CalibrationState.START;
                stop();
                break;
        }
    }

    public void start() {
        Log.d(TAG, "Start Start");
        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), DELAY); // ,SensorManager.SENSOR_DELAY_NORMAL);
        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD), DELAY); // ,SensorManager.SENSOR_DELAY_NORMAL);
    }

    public void stop() {
        Log.d(TAG, "Start Stop");
        sensorManager.unregisterListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER));
        sensorManager.unregisterListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD));
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // ...
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        switch (event.sensor.getType()) {
            case Sensor.TYPE_MAGNETIC_FIELD:
                mags = event.values.clone();
                break;
            case Sensor.TYPE_ACCELEROMETER:
                accels = event.values.clone();
                break;
        }

        if (mags != null && accels != null) {
            gravRot = new float[9];
            magRot = new float[9];
            SensorManager.getRotationMatrix(gravRot, magRot, accels, mags);

            float[] outR = new float[9];
            SensorManager.remapCoordinateSystem(gravRot, SensorManager.AXIS_X, SensorManager.AXIS_Z, outR);
            SensorManager.getOrientation(outR, values);

            // azimuth = values[0]; //  * 57.2957795f; //looks like we don't need this one
            pitch = values[1];  //  * 57.2957795f;
            roll = values[2]; //  * 57.2957795f;
            mags = null;      // retrigger the loop when things are repopulated
            accels = null;   // retrigger the loop when things are repopulated

            updateSpeedTurnUI.run();

        }
    }

    public Runnable updateSpeedTurnUI = new Runnable() {
        @Override
        public void run() {
            pitchText.setText("pitch: " + pitch);
            rollText.setText("roll: " + roll);
        }
    };
}
