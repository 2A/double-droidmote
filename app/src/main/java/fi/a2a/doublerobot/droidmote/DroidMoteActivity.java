/*
 * Copyright (c) 2014, Ericsson AB. All rights reserved.
 * Copyright (c) 2016, 2017, 2018, Antti E. Ainasoja.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this
 * list of conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 */

package fi.a2a.doublerobot.droidmote;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.MotionEvent;
import android.view.TextureView;
import android.view.View;
// import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.ericsson.research.owr.Owr;
import com.ericsson.research.owr.sdk.CameraSource;
import com.ericsson.research.owr.sdk.InvalidDescriptionException;
import com.ericsson.research.owr.sdk.RtcCandidate;
import com.ericsson.research.owr.sdk.RtcCandidates;
import com.ericsson.research.owr.sdk.RtcConfig;
import com.ericsson.research.owr.sdk.RtcConfigs;
import com.ericsson.research.owr.sdk.RtcSession;
import com.ericsson.research.owr.sdk.RtcSessions;
import com.ericsson.research.owr.sdk.SessionDescription;
import com.ericsson.research.owr.sdk.SessionDescriptions;
import com.ericsson.research.owr.sdk.SimpleStreamSet;
import com.ericsson.research.owr.sdk.VideoView;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONException;
import org.json.JSONObject;

import android.graphics.Matrix;

public class DroidMoteActivity extends Activity implements
        SignalingChannel.JoinListener,
        SignalingChannel.DisconnectListener,
        SignalingChannel.SessionFullListener,
        SignalingChannel.MessageListener,
        SignalingChannel.PeerDisconnectListener,
        RtcSession.OnLocalCandidateListener,
        RtcSession.OnLocalDescriptionListener,
        View.OnTouchListener {

    private static final String TAG = "DroidMoteActivity";

    private static final int PERMISSION_REQ_CAM = 1;
    private static final int PERMISSION_REQ_MIC = 2;
    private static final int PERMISSION_REQ_VIB = 3;


    private SharedPreferences sharedPreferences;

    /**
     * Initialize OpenWebRTC at startup
     */
    static {
        Log.d(TAG, "Initializing OpenWebRTC");
        Owr.init();
        Owr.runInBackground();
    }

    private Button mSettingsButton;
    private Button mJoinButton;
    private Button mCallButton;

    private Button mParkSwitch;

    private Button mLDMSwitch;
    private Button mRDMSwitch;

    private TextView pitchText;
    private TextView speedText;
    private TextView rollText;
    private TextView turnText;
    private TextView pingText;
    private EditText mUserInput;
    private EditText mSessionInput;
    private CheckBox mAudioCheckBox;
    private CheckBox mVideoCheckBox;

    private TextureView mRemoteTextureView;

    private SignalingChannel mSignalingChannel;
    private InputMethodManager mInputMethodManager;
    // private WindowManager mWindowManager;
    private SignalingChannel.PeerChannel mPeerChannel;
    private RtcSession mRtcSession;
    private SimpleStreamSet mStreamSet;
    private VideoView mSelfView;
    private VideoView mRemoteView;
    private RtcConfig mRtcConfig;

    private Tracker tracker;
    private long ping;

    private boolean LDMSwitchOn;
    private boolean RDMSwitchOn;
    private boolean parkStatus;
    private int kickStandState;
    int DMSwitchColor;

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Check permissions
        checkPermissions();

        parkStatus = false;
        ping = 0;
        kickStandState = 0;

        // Load preferences
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());

        // for toggling virtual keyboard
        mInputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);

        // Initialize UI elements
        initUi();

        // Forcing full screen mode / hiding title bar
        // mWindowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        // requestWindowFeature(Window.FEATURE_NO_TITLE);
        // getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
        //         WindowManager.LayoutParams.FLAG_FULLSCREEN);

        // Tilt Control
        LDMSwitchOn = false;
        RDMSwitchOn = false;
        tracker = new Tracker(this, this.getApplicationContext());

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    // CHECK PERMISSIONS
    // check permissions to use camera and microphone, ask them if they have not been given already
    private void checkPermissions() {
        int cameraCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        int micCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO);
        int vibCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.VIBRATE);

        if (cameraCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, PERMISSION_REQ_CAM);
        }
        if (micCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECORD_AUDIO}, PERMISSION_REQ_MIC);
        }
        if (vibCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.VIBRATE}, PERMISSION_REQ_VIB);
        }
    }
    // Callback for checking permissions. Application is closed if permissions are not granted.
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQ_CAM: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission granted
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setIcon(android.R.drawable.ic_dialog_alert)
                            .setTitle("Closing Application")
                            .setCancelable(false)
                            .setMessage("Application requires permission to use camera to run.")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    System.exit(0);
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
                return;
            }
            case PERMISSION_REQ_MIC: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission granted
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setIcon(android.R.drawable.ic_dialog_alert)
                            .setTitle("Closing Application")
                            .setCancelable(false)
                            .setMessage("Application requires permission to use microphone to run.")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    // finish();
                                    System.exit(0);
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
                return;
            }
            case PERMISSION_REQ_VIB: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission granted
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setIcon(android.R.drawable.ic_dialog_alert)
                            .setTitle("Closing Application")
                            .setCancelable(false)
                            .setMessage("Application requires vibration for haptic feedback.")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    // finish();
                                    System.exit(0);
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
                return;
            }
        }
    }

    @Override
    public void onConfigurationChanged(final Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        initUi();
        updateVideoView(true);
    }

    private void updateVideoView(boolean running) {
        if (mStreamSet != null) {
            TextureView selfView = (TextureView) findViewById(R.id.self_view);
            TextureView remoteView = (TextureView) findViewById(R.id.remote_view);
            selfView.setVisibility(running ? View.VISIBLE : View.INVISIBLE);
            remoteView.setVisibility(running ? View.VISIBLE : View.INVISIBLE);
            if (running) {
                Log.d(TAG, "setting self-view: " + selfView);
                mSelfView.setView(selfView);
                mRemoteView.setView(remoteView);
                // enable remote control start/stop buttons
                mLDMSwitch.setEnabled(true);
                mRDMSwitch.setEnabled(true);
                // mStartButton.setEnabled(true);
                // mStopButton.setEnabled(true);

                //            mStreamSet.setDeviceOrientation(mWindowManager.getDefaultDisplay().getRotation());
            } else {
                Log.d(TAG, "stopping self-view");
                mSelfView.stop();
                mRemoteView.stop();

                mLDMSwitch.setEnabled(false);
                mRDMSwitch.setEnabled(false);

                // mStartButton.setEnabled(false);
                // mStopButton.setEnabled(false);
            }
        }
    }

    public void initUi() {

        setContentView(R.layout.activity_droidmote);

        // Settings button
        mSettingsButton = (Button) findViewById(R.id.settingsButton);

        // WebRTC connection UI
        mUserInput = (EditText) findViewById(R.id.user_id);
        mSessionInput = (EditText) findViewById(R.id.session_id);
        mAudioCheckBox = (CheckBox) findViewById(R.id.audio);
        mVideoCheckBox = (CheckBox) findViewById(R.id.video);
        mCallButton = (Button) findViewById(R.id.call);
        mJoinButton = (Button) findViewById(R.id.join);
        mJoinButton.setEnabled(true);


        // Debug info UI
        pitchText = (TextView) findViewById(R.id.pitchText);
        speedText = (TextView) findViewById(R.id.speedText);
        rollText = (TextView) findViewById(R.id.rollText);
        turnText = (TextView) findViewById(R.id.turnText);
        pingText = (TextView) findViewById(R.id.pingText);
        // Tilt control UI
        mParkSwitch = (Button) findViewById(R.id.startStop);
        mLDMSwitch = (Button) findViewById(R.id.leftDeadManSwitch);
        mRDMSwitch = (Button) findViewById(R.id.rightDeadManSwitch);
        DMSwitchColor = Color.BLUE;
        mLDMSwitch.setOnTouchListener(this);
        mRDMSwitch.setOnTouchListener(this);

        // RTC video UI
        mRemoteTextureView = (TextureView) findViewById(R.id.remote_view);

        //mHeader = findViewById(R.id.header);
        // mHeader.setCameraDistance(getResources().getDisplayMetrics().widthPixels * 5);
        // mHeader.setPivotX(getResources().getDisplayMetrics().widthPixels / 2);
        // mHeader.setPivotY(0);
        // mSettingsHeader = findViewById(R.id.settings_header);
        // mSettingsHeader.setCameraDistance(getResources().getDisplayMetrics().widthPixels * 5);
        // mSettingsHeader.setPivotX(getResources().getDisplayMetrics().widthPixels / 2);
        // mSettingsHeader.setPivotY(0);

        /*
        mUrlSetting = (EditText) findViewById(R.id.url_setting);
        mUrlSetting.setText(getUrl());
        mUrlSetting.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(final TextView view, final int actionId, final KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    hideSettings();
                    String url = view.getText().toString();
                    saveUrl(url);
                    return true;
                }
                return false;
            }
        });

        mSTUNSetting = (EditText) findViewById(R.id.stun_setting);
        mSTUNSetting.setText(getStun());
        mSTUNSetting.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(final TextView view, final int actionId, final KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    hideSettings();
                    String url = view.getText().toString();
                    saveStun(url);
                    return true;
                }
                return false;
            }
        });
        */
    }

    public void onSelfViewClicked(final View view) {
        Log.d(TAG, "onSelfViewClicked");
        if (mStreamSet != null) {
            if (mSelfView != null) {
                mSelfView.setRotation((mSelfView.getRotation() + 1) % 4);
            }
        }
//        mStreamSet.toggleCamera();
    }

    private void adjustAspectRatio() {
        int viewWidth = mRemoteTextureView.getWidth();
        int viewHeight = mRemoteTextureView.getHeight();
        double aspectRatio = (double) viewWidth / viewHeight;

        int newWidth, newHeight;

        // if (viewHeight > (int) (viewWidth * aspectRatio)) {
        // limited by narrow width; restrict height
        newWidth = (int) (viewHeight / aspectRatio);
        newHeight = (int) (viewHeight / aspectRatio);

        // newHeight = (int) (viewWidth * aspectRatio);
        // } else {

        // limited by short height; restrict width
        //     newWidth = (int) (viewHeight / aspectRatio);
        //  newHeight = viewHeight;
        // }
        int xoff = (viewWidth - newWidth) / 2;
        int yoff = (viewHeight - newHeight) / 2;
        Log.v(TAG, "video=" + viewHeight + "x" + viewWidth +
                " view=" + viewWidth + "x" + viewHeight +
                " newView=" + newWidth + "x" + newHeight +
                " off=" + xoff + "," + yoff);

        Matrix txform = new Matrix();
        mRemoteTextureView.getTransform(txform);
        txform.setScale((float) newWidth / viewWidth, (float) newHeight / viewHeight);
        txform.postTranslate(xoff, yoff);
        mRemoteTextureView.setTransform(txform);
    }

    public void onJoinClicked(final View view) {
        Log.d(TAG, "onJoinClicked");


        mSettingsButton.setEnabled(false);
        // WebRTCinit
        mRtcConfig = RtcConfigs.defaultConfig(getStun());

        String userId =  mUserInput.getText().toString();
        if (userId.isEmpty()) {
            mUserInput.requestFocus();
            mInputMethodManager.showSoftInput(mSessionInput, InputMethodManager.SHOW_IMPLICIT);
            return;
        }
        String sessionId = mSessionInput.getText().toString();
        if (sessionId.isEmpty()) {
            mSessionInput.requestFocus();
            mInputMethodManager.showSoftInput(mSessionInput, InputMethodManager.SHOW_IMPLICIT);
            return;
        }

        mInputMethodManager.hideSoftInputFromWindow(mSessionInput.getWindowToken(), 0);
        mUserInput.setEnabled(false);
        mSessionInput.setEnabled(false);
        mJoinButton.setEnabled(false);
        mAudioCheckBox.setEnabled(false);
        mVideoCheckBox.setEnabled(false);

        mSignalingChannel = new SignalingChannel(getUrl(), sessionId, userId);
        mSignalingChannel.setJoinListener(this);
        mSignalingChannel.setDisconnectListener(this);
        mSignalingChannel.setSessionFullListener(this);

        boolean wantAudio = mAudioCheckBox.isChecked();
        boolean wantVideo = mVideoCheckBox.isChecked();
        mStreamSet = SimpleStreamSet.defaultConfig(wantAudio, wantVideo);
        mSelfView = CameraSource.getInstance().createVideoView();
        mRemoteView = mStreamSet.createRemoteView();
        // int vW_ = mRemoteTextureView.getWidth();
        int vH_ = mRemoteTextureView.getHeight();
        mRemoteTextureView.setRotation(90);
        int vH2_ = mRemoteTextureView.getHeight();
        mRemoteTextureView.setTranslationY(-(150));
        // adjustAspectRatio();
        updateVideoView(true);
    }

    @Override
    public void onPeerJoin(final SignalingChannel.PeerChannel peerChannel) {
        Log.v(TAG, "onPeerJoin => " + peerChannel.getPeerId());
        mCallButton.setEnabled(true);
        mPeerChannel = peerChannel;

        tracker.setPeerChannel(mPeerChannel);

        mPeerChannel.setDisconnectListener(this);
        mPeerChannel.setMessageListener(this);

        mRtcSession = RtcSessions.create(mRtcConfig);
        mRtcSession.setOnLocalCandidateListener(this);
        mRtcSession.setOnLocalDescriptionListener(this);
    }

    @Override
    public void onPeerDisconnect(final SignalingChannel.PeerChannel peerChannel) {
        Log.d(TAG, "onPeerDisconnect => " + peerChannel.getPeerId());
        mRtcSession.stop();
        mPeerChannel = null;
        tracker.setPeerChannel(mPeerChannel);
        updateVideoView(false);
        mSessionInput.setEnabled(true);
        mJoinButton.setEnabled(true);
        mCallButton.setEnabled(false);
        mAudioCheckBox.setEnabled(true);
        mVideoCheckBox.setEnabled(true);
    }

    @Override
    public synchronized void onMessage(final JSONObject json) {
        if (json.has("data")) {
            JSONObject data = json.optJSONObject("data");
            Log.v(TAG, "data: " + data);
            if (data.has("ping")) {
                long pingback = data.optLong("ping");
                long ptime = System.currentTimeMillis();
                ping = ptime - pingback;
                Log.d(TAG, "ping: " + Long.toString(ping));
            }
            if (data.has("status")) {
                JSONObject status = data.optJSONObject("status");
                if (status.has("stand_state")) {
                    kickStandState = status.optInt("stand_state");
                    Log.d(TAG, "Kickstand: " + Integer.toString(kickStandState));
                    // Set kickstand state
                    switch (kickStandState) {
                        case(1): // deployed
                            parkStatus = false;
                            mParkSwitch.setText("START");
                            mParkSwitch.setEnabled(true);
                            break;
                        case(2):
                            parkStatus = true;
                            mParkSwitch.setText("PARK");
                            mParkSwitch.setEnabled(true);
                            break;
                        case(3):
                        case(4):
                            mParkSwitch.setText("- - -");
                            mParkSwitch.setEnabled(false);
                            break;

                    }

                }
            }
            /*
            try {
                String msg = jdata.getString("message") + "\n";
                rmsgstr = jdata.getString("message") + " back!";
                Log.i(TAG, msg);
                // ((Editable) mChatBox.getText()).insert(mChatBox.getText().length(), msg);
            } catch(Exception e) {
                Log.e(TAG, "ERROR HORROR!");
            }
            try {
                JSONObject rjson = new JSONObject();
                JSONObject rmsg = new JSONObject();
                rmsg.put("message", rmsgstr);
                rjson.put("data", rmsg);
                mPeerChannel.send(rjson);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            */
        }
        if (json.has("candidate")) {
            JSONObject candidate = json.optJSONObject("candidate");
            Log.v(TAG, "candidate: " + candidate);
            RtcCandidate rtcCandidate = RtcCandidates.fromJsep(candidate);
            if (rtcCandidate != null) {
                mRtcSession.addRemoteCandidate(rtcCandidate);
            } else {
                Log.w(TAG, "invalid candidate: " + candidate);
            }
        }
        if (json.has("sdp") || json.has("sessionDescription")) {
            Log.v(TAG, "sdp: " + json);
            try {
                SessionDescription sessionDescription = SessionDescriptions.fromJsep(json);
                if (sessionDescription.getType() == SessionDescription.Type.OFFER) {
                    onInboundCall(sessionDescription);
                } else {
                    onAnswer(sessionDescription);
                }
            } catch (InvalidDescriptionException e) {
                e.printStackTrace();
            }
        }
        if (json.has("orientation")) {
//                handleOrientation(json.getInt("orientation"));
        }
    }

    @Override
    public void onLocalCandidate(final RtcCandidate candidate) {
        if (mPeerChannel != null) {
            try {
                JSONObject json = new JSONObject();
                json.putOpt("candidate", RtcCandidates.toJsep(candidate));
                Log.d(TAG, "sending candidate: " + json);
                mPeerChannel.send(json);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void onCallClicked(final View view) {
        Log.d(TAG, "onCallClicked");

        mRtcSession.start(mStreamSet);
        mCallButton.setEnabled(false);
    }

    public void onStartStopClicked(final View view) {
        Log.d(TAG, "onStartStopClicked");

        if (parkStatus) {
            parkStatus = false;
            mParkSwitch.setText("START");
            tracker.park();
            tracker.stop();
        }
        else {
            parkStatus = true;
            mParkSwitch.setText("PARK");
            tracker.unpark();
            // tracker.start();
        }
    }
    private void onInboundCall(final SessionDescription sessionDescription) {
        try {
            mRtcSession.setRemoteDescription(sessionDescription);
            mRtcSession.start(mStreamSet);
            mParkSwitch.setEnabled(true);
        } catch (InvalidDescriptionException e) {
            e.printStackTrace();
        }
    }

    private void onAnswer(final SessionDescription sessionDescription) {
        if (mRtcSession != null) {
            try {
                mRtcSession.setRemoteDescription(sessionDescription);
                mParkSwitch.setEnabled(true);
            } catch (InvalidDescriptionException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onLocalDescription(final SessionDescription localDescription) {
        if (mPeerChannel != null) {
            JSONObject json = SessionDescriptions.toJsep(localDescription);
            Log.d(TAG, "sending sdp: " + json);
            mPeerChannel.send(json);
        }
    }

    @Override
    public void onDisconnect() {
        Toast.makeText(this, "Disconnected from server", Toast.LENGTH_LONG).show();
        updateVideoView(false);
        mStreamSet = null;
        if (mRtcSession != null) {
            Log.d(TAG, "RTC NOT NULL!");

            mRtcSession.stop();
        } else {
            Log.d(TAG, "RTC IS NULL!");
        }
        mRtcSession = null;
        mSignalingChannel = null;
    }

    @Override
    public void onSessionFull() {
        Toast.makeText(this, "Session is full", Toast.LENGTH_LONG).show();
        mJoinButton.setEnabled(true);
    }

    public void onSettingsClicked(final View view) {
        Intent i = new Intent(this, SettingsActivity.class);
        startActivity(i);
    }

    public void onCancelSettingsClicked(final View view) {
        // hideSettings();
    }

    private String getUrl() {
        return sharedPreferences.getString("pref_server_url", Config.DEFAULT_SERVER_ADDRESS);
    }

    private String getStun() {
        return sharedPreferences.getString("pref_stun", Config.DEFAULT_SERVER_ADDRESS);
    }

    /**
     * Shutdown the process as a workaround until cleanup has been fully implemented.
     */
    @Override
    protected void onStop() {
        Log.d(TAG, "Stopping..");
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        // finish();
        // System.exit(0);
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.disconnect();
    }

    @Override
    protected void onPause() {
        Log.d(TAG, "Pausing..");
        super.onPause();
    }

    @Override
    protected void onResume() {
        Log.d(TAG, "server: " + sharedPreferences.getString("pref_server_url", Config.DEFAULT_SERVER_ADDRESS));
        super.onResume();
    }

    public Runnable updateSpeedTurnUI = new Runnable() {
        @Override
        public void run() {
            pitchText.setText("pitch: " + tracker.pitch);
            rollText.setText("roll: " + tracker.roll);
            speedText.setText("speed: " + tracker.speed);
            if (tracker.speed < 0.0) speedText.setTextColor(Color.RED);
            else if (tracker.speed > 0.0) speedText.setTextColor(Color.GREEN);
            else {
                speedText.setTextColor(Color.YELLOW);
            }
            turnText.setText("turn: " + tracker.turn);
            if (tracker.turn < 0.0) turnText.setTextColor(Color.RED);
            else if (tracker.turn > 0.0) turnText.setTextColor(Color.GREEN);
            else {
                turnText.setTextColor(Color.YELLOW);
            }
            pingText.setText("ping: " + ping + " ms");

        }
    };

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        int eID = v.getId();
        if (eID == mLDMSwitch.getId()) {
            // Log.d(TAG, MotionEvent.actionToString(event.getAction()) );
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                Log.d(TAG, "LEFT SWITCH ON");
                LDMSwitchOn = true;
                if (LDMSwitchOn && RDMSwitchOn) {
                    mLDMSwitch.setBackgroundColor(Color.GREEN);
                    mRDMSwitch.setBackgroundColor(Color.GREEN);
                    tracker.start();
                }
                else {
                    mLDMSwitch.setBackgroundColor(Color.YELLOW);
                    // tracker.unpark();
                }
            }
            if (event.getAction() == MotionEvent.ACTION_UP) {
                Log.d(TAG, "LEFT SWITCH OFF");
                tracker.stop();
                LDMSwitchOn = false;
                if (RDMSwitchOn) { mRDMSwitch.setBackgroundColor(Color.YELLOW); }
                else {
                    mRDMSwitch.setBackgroundColor(Color.CYAN);
                    tracker.stop();
                }
                mLDMSwitch.setBackgroundColor(Color.CYAN);
            }
        }
        if (eID == mRDMSwitch.getId()) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                Log.d(TAG, "RIGHT SWITCH ON");
                RDMSwitchOn = true;
                if (LDMSwitchOn && RDMSwitchOn) {
                    mLDMSwitch.setBackgroundColor(Color.GREEN);
                    mRDMSwitch.setBackgroundColor(Color.GREEN);
                    tracker.start();
                }
                else {
                    mRDMSwitch.setBackgroundColor(Color.YELLOW);
                    // tracker.unpark();
                }
            }
            if (event.getAction() == MotionEvent.ACTION_UP) {
                Log.d(TAG, "RIGHT SWITCH OFF");
                tracker.stop();
                RDMSwitchOn = false;
                if (LDMSwitchOn) { mLDMSwitch.setBackgroundColor(Color.YELLOW); }
                else {
                    mLDMSwitch.setBackgroundColor(Color.CYAN);
                    tracker.stop();
                }
                mRDMSwitch.setBackgroundColor(Color.CYAN);
            }
        }
        return true;
    }

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("DroidMote Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }
}
