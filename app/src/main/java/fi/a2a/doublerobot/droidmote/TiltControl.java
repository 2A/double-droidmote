package fi.a2a.doublerobot.droidmote;

import android.Manifest;
import android.app.Activity;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.MotionEvent;
import android.view.TextureView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.ericsson.research.owr.sdk.RtcConfigs;
import com.ericsson.research.owr.sdk.VideoView;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

/**
 * Created by doublea on 18.1.2017.
 */

public class TiltControl extends Activity implements
        View.OnTouchListener
{
    private static final String TAG = "TiltControl";

    private Tracker tracker;
    private SharedPreferences sharedPreferences;

    // Control UI
    private Button mLDMSwitch;
    private Button mRDMSwitch;
    private boolean LDMSwitchOn;
    private boolean RDMSwitchOn;
    private VideoView mSelfView;
    private VideoView mRemoteView;

    // Debug data
    private TextView pitchText;
    private TextView speedText;
    private TextView rollText;
    private TextView turnText;

    //////
    private InputMethodManager mInputMethodManager;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        LDMSwitchOn = false;
        RDMSwitchOn = false;
        initUi();

        // Load preferences
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());

        mInputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);

        // tracker = new Tracker(this, this.getApplicationContext());
        // tracker.setUICallBack(updateSpeedTurnUI);
    }

    public void initUi() {
        // setContentView(R.layout.activity_openwebrtc);
        setContentView(R.layout.tilt_control_layout);

        // mStartButton = (Button) findViewById(R.id.Start);
        // mStopButton = (Button) findViewById(R.id.Stop);
        mLDMSwitch = (Button) findViewById(R.id.leftDeadManSwitch);
        mRDMSwitch = (Button) findViewById(R.id.rightDeadManSwitch);
        mLDMSwitch.setOnTouchListener(this);
        mRDMSwitch.setOnTouchListener(this);

        pitchText = (TextView) findViewById(R.id.pitchText);
        speedText = (TextView) findViewById(R.id.speedText);
        rollText = (TextView) findViewById(R.id.rollText);
        turnText = (TextView) findViewById(R.id.turnText);
        /*
        mRemoteTextureView = (TextureView) findViewById(R.id.remote_view);

        mHeader.setCameraDistance(getResources().getDisplayMetrics().widthPixels * 5);
        mHeader.setPivotX(getResources().getDisplayMetrics().widthPixels / 2);
        mHeader.setPivotY(0);
        mSettingsHeader = findViewById(R.id.settings_header);
        mSettingsHeader.setCameraDistance(getResources().getDisplayMetrics().widthPixels * 5);
        mSettingsHeader.setPivotX(getResources().getDisplayMetrics().widthPixels / 2);
        mSettingsHeader.setPivotY(0);
*/
        /*
        mUrlSetting = (EditText) findViewById(R.id.url_setting);
        mUrlSetting.setText(getUrl());
        mUrlSetting.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(final TextView view, final int actionId, final KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    hideSettings();
                    String url = view.getText().toString();
                    saveUrl(url);
                    return true;
                }
                return false;
            }
        });

        mSTUNSetting = (EditText) findViewById(R.id.stun_setting);
        mSTUNSetting.setText(getStun());
        mSTUNSetting.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(final TextView view, final int actionId, final KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    hideSettings();
                    String url = view.getText().toString();
                    saveStun(url);
                    return true;
                }
                return false;
            }
        });
        */
    }

    public void startStop() {
        if (LDMSwitchOn && RDMSwitchOn) {
            tracker.start();
        } else {
            tracker.stop();
        }
    }
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        int eID = v.getId();
        if (eID == mLDMSwitch.getId()) {
            // Log.d(TAG, MotionEvent.actionToString(event.getAction()) );
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                Log.d(TAG, "LEFT SWITCH ON");
                mLDMSwitch.setBackgroundColor(Color.GREEN);
                LDMSwitchOn = true;
                startStop();
            }
            if (event.getAction() == MotionEvent.ACTION_UP) {
                Log.d(TAG, "LEFT SWITCH OFF");
                LDMSwitchOn = false;
                mLDMSwitch.setBackgroundColor(Color.CYAN);

                startStop();
            }
        }
        if (eID == mRDMSwitch.getId()) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                Log.d(TAG, "RIGHT SWITCH ON");
                RDMSwitchOn = true;
                mRDMSwitch.setBackgroundColor(Color.GREEN);

                startStop();
            }
            if (event.getAction() == MotionEvent.ACTION_UP) {
                Log.d(TAG, "RIGHT SWITCH OFF");
                RDMSwitchOn = false;
                mRDMSwitch.setBackgroundColor(Color.CYAN);
                startStop();
            }
        }
        return true;
    }
}
